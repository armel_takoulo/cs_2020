package org.imt.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.util.logging.Logger;

import javax.inject.Inject;
//import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.imt.helpers.ResourceFileHelper;
//import org.imt.websocket.HomeEndpoint;





//import org.fusesource.mqtt.cli.Publisher;


//see http://zetcode.com/java/jetty/websocket/

@WebServlet({ "/helo" })
public class IotServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final boolean LOG = true; 

	//private static final boolean USE_WEBSOCKET_ = true; // FP191203

	private static DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);


	// @Inject
	// private HiveDaoBean hiveDao;

	@Inject
	private Logger log;
	//CDI Context and dependency Injection

	  
	
	public void clog(String mesg) {
		if (LOG)
			//log.info(mesg);
		  System.out.println(mesg);
	}

	private static void sclog(String m) {
		System.out.println(m);
	}


	//@Inject
	//private HomeEndpoint wsServerEndpoint;
	
	

	@Override
	public void init() throws ServletException {
		super.init();
		ResourceFileHelper.getInstance().setUploadDir(this.getServletContext().getRealPath("/"));
	}

	


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
	   if ("/helo".equals(path)) {
			response.getWriter().println("hi");
		}
	}
}








