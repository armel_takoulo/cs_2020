/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/
package org.connecthive.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/*
import java.util.GregorianCalendar;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;*/

/*---------- generated  User --------------*/
@Entity
@Table(name = "User")
public class User implements java.io.Serializable {
	
	private static final String pattern = "yyyy-MM-dd";
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private String foreName;
	private String email;
	private String nickName;
	private String password;
	
//https://vladmihalcea.com/date-timestamp-jpa-hibernate/
	
	private Date birthDate;
	//private ZonedDateTime birthDate;
	
	
	//@OneToMany(mappedBy = "emitter_", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//@JsonIgnore
	//private List<Message> messages;
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "company_fk")
	private Company company;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setForeName(String foreName) {
		this.foreName = foreName;
	}

	public String getForeName() {
		return foreName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
	
	
	@JsonIgnore
	public Date getBirthDate() {
		return birthDate;
	}

	@JsonIgnore
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	

	
	public String getBirthDateString() {
		if (birthDate!=null)
		    return simpleDateFormat.format(birthDate);
		else
			return "";
	}

	
	public void setBirthDateString(String birthDateString) {
		Date date;
		try {
			date = simpleDateFormat.parse(birthDateString);
			setBirthDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	}

/*
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public List<Message> getMessages() {
		return messages;
	}
*/
	public void setCompany(Company company) {
		this.company = company;
	}

	public Company getCompany() {
		return company;
	}
}