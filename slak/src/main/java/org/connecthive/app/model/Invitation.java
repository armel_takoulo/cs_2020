/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/
package org.connecthive.app.model;
  

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;

/*---------- generated  Invitation --------------*/
@Entity
@NamedQueries(
{ 
   @NamedQuery(name = "allInvitationsByUser", query = "select a FROM Invitation a where a.emitter.id = :user_id"),
   @NamedQuery(name = "allInvitationsForUser", query = "select a FROM Invitation a where a.receiver.id = :user_id")
})
@Table(name="Invitation")
public class Invitation implements java.io.Serializable{

    @Id
    @GeneratedValue
    private Integer id;
    private String message;
    private Date invitationDate;
    private Date joinDate;
    @ManyToOne
   // @JsonBackReference(value="emitter_b")
    @JoinColumn(name = "emitter_fk")
    private User emitter;
    @ManyToOne
  //  @JsonBackReference(value="receiver_b")
    @JoinColumn(name = "receiver_fk")
    private User receiver;
    
    
    @ManyToOne
    //@JsonBackReference(value="channel_b")
    @JoinColumn(name = "channel_fk")
    private Channel channel;
    

    public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public void setId(Integer id){
       this.id = id;
    }
    
    public Integer getId(){
        return id;
    }

    public void setMessage(String message){
       this.message = message;
    }
    
    public String getMessage(){
        return message;
    }

    public void setInvitationDate(Date invitationDate){
       this.invitationDate = invitationDate;
    }
    
    public Date getInvitationDate(){
        return invitationDate;
    }

    public void setJoinDate(Date joinDate){
       this.joinDate = joinDate;
    }
    
    public Date getJoinDate(){
        return joinDate;
    }

    public void setEmitter(User emitter){
       this.emitter = emitter;
    }
    
    public User getEmitter(){
        return emitter;
    }

    public void setReceiver(User receiver){
       this.receiver = receiver;
    }
    
    public User getReceiver(){
        return receiver;
    }
}

//TODO relation w channel