package org.imt.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.logging.Logger;
import java.util.List;

import javax.inject.Inject;
//import javax.inject.Inject;
//import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.connecthive.app.dao.ChannelDao;
import org.connecthive.app.dao.CompanyDao;
import org.connecthive.app.dao.InvitationDao;
import org.connecthive.app.dao.MessageDao;
import org.connecthive.app.dao.UserDao;
import org.connecthive.app.model.Channel;
import org.connecthive.app.model.Invitation;
import org.connecthive.app.model.Message;
import org.connecthive.app.model.User;
import org.imt.helpers.ResourceFileHelper;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//import org.fusesource.mqtt.cli.Publisher;

//see http://zetcode.com/java/jetty/websocket/

@WebServlet({ "/helo", "/test7845", "/populate1254" })
public class SlakServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final boolean LOG = true;
	private static final Logger log = Logger.getLogger(SlakServlet.class.getName());

	// private static final boolean USE_WEBSOCKET_ = true; // FP191203

	private static DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
	
	String pattern = "yyyy-MM-dd";
	//String pattern = "dd-MM-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	

	@Inject
	private UserDao userDao;

	@Inject
	private InvitationDao invitationDao;

	@Inject
	private ChannelDao channelDao;

	@Inject
	private CompanyDao companyDao;

	@Inject
	private MessageDao messageDao;

	public void clog(String mesg) {
		if (LOG)
			log.info(mesg);
		// System.out.println(mesg);
	}

	private static void sclog(String m) {
		System.out.println(m);
	}

	@Override
	public void init() throws ServletException {
		super.init();
		ResourceFileHelper.getInstance().setUploadDir(this.getServletContext().getRealPath("/"));
		clog("init ok");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
		if ("/helo".equals(path)) {
			response.getWriter().println("hi");
			List<User> users = userDao.getUsers();
			if (users.isEmpty())
				 populate();
		}
	    else
			response.getWriter().println("unknown");
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
        if ("/populate1254".equals(path)) {
			String result = populate();
			response.getWriter().println("populate done : " + result);
		} else if ("/test7845".equals(path)) {
			String result = testMessage();
			response.getWriter().println("test done : " + result);
		} else
			response.getWriter().println("unknown");
	}
	

	private String populate() {
		clog("populating");
		messageDao.deleteAll();
		invitationDao.deleteAll();
		channelDao.deleteAll();
		userDao.deleteAll();
		User usermartin = createUser("Martin", "Peter", "johnny", "qwerty", "ji@jiji.com","1995-02-06");
		User usersmith = createUser("Smith", "Jo", "jamesbond", "azerty", "jo@jojo.com","2005-10-14");
		User usertom = createUser("Tom", "Tim", "ken", "foo", "ko@kojo.com","2002-11-27");
		Channel channel_dev = createChannel("Development", "(dev) this channel is about dev", false, "2020-05-01");
		Channel channel_devops = createChannel("Devops", "(devops) this channel is about devops", true, "2019-04-02");
		Invitation invitation1 = sendInvitation(usermartin, usersmith, "Hello Jo Smith join Slak - channel dev");
		invitation1.setChannel(channel_dev);
		invitationDao.merge(invitation1);
		Invitation invitation2 = sendInvitation(usersmith, usermartin, "Hello Peter Martin join Slak - channel devops");
		invitation2.setChannel(channel_devops);
		invitationDao.merge(invitation2);
		Invitation invitation3 = sendInvitation(usersmith, usertom, "Hello Tim Tom join Slak - channel dev");
		invitation3.setChannel(channel_dev);
		invitationDao.merge(invitation3);
		
		Message message_martin_dev1 = sendMessage(usermartin, channel_dev, "(1ma) this is the message content 1 about java foo", "DEV_JAVA","2020-05-01");
		Message message_smith_dev02 = sendMessage(usersmith, channel_dev, "(2sm) this is the message content 2 about java bar", "DEV_JAVA","2020-05-02");
		Message message_martin_devops3 = sendMessage(usermartin, channel_devops, "(3ma) this is the message content 3 about java baz", "OPS_JAVA","2020-05-03");
		Message message_martin_dev4 = sendMessage(usermartin, channel_dev, "(4ma) this is the message content 4 about java djin", "DEV_JAVA","2020-05-04");
		Message message_smith_devops1 = sendMessage(usersmith, channel_devops, "(5sm) this is the message content 1 about node foo", "OPS_NODE","2020-05-05");
		Message message_smith_dev2b = sendMessage(usersmith, channel_dev, "(6sm) this is the message content 2 about node foo", "DEV_NODE","2020-05-06");
		clog("populate ok");
		return "populate ok";
	}

	private String testMessage() {
		String json = "{\"testMessage\",\"error\"}";
		User u = userDao.findByEmail("ji@jiji.com");
		Channel c = channelDao.findByName("Development");
		Message message = sendMessage(u, c, "the test2 is ok", "DEV_JAVA","2020-05-07");
		try {
			json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(message);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}

//"Development","this channel is about dev",false,
	private Channel createChannel(String name, String description, boolean privat, String creationDate) {
		Channel channel = new Channel();
		channel.setName(name);
		channel.setDescription(description);
		
		try {
			channel.setDateCreation(simpleDateFormat.parse(creationDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		channel.setPrivat(privat);
		channelDao.persist(channel);
		return channel;
	}

	private User createUser(String name, String forename, String nickname, String password, String email, String birthDate) {
		User user = new User();
		user.setName(name);
		user.setForeName(forename);
		user.setNickName(nickname);
		user.setPassword(password);
		user.setEmail(email);
		try {
			user.setBirthDate(simpleDateFormat.parse(birthDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		userDao.persist(user);
		return user;
	}


	private Invitation sendInvitation(User from, User to, String message) {
		Invitation invitation = new Invitation();
		invitation.setMessage(message);
		invitation.setInvitationDate(new Date());
		invitation.setEmitter(from);
		invitation.setReceiver(to);
		invitationDao.persist(invitation);
		return invitation;
	}


	private Message sendMessage(User from, Channel channel, String content, String type, String date) {
		Message message = new Message();
		message.setChannel(channel);
		message.setContent(content);
		try {
			message.setDate(simpleDateFormat.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		message.setEmitter(from);
		message.setType(type);
		messageDao.persist(message);
		return message;
	}

	private String test1() {
		String json = "{}";
		ObjectMapper objectMapper = new ObjectMapper();
		List<User> users = userDao.findAllOrderedByName();
		for (User user : users) {
			try {
				json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
				clog(json);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // superf);

		}
		return json;

	}
}

//see https://cassiomolin.com/2016/03/23/why-you-should-use-dtos-in-your-rest-api/