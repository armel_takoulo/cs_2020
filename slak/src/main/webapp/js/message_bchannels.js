

/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for Message by Channel  --------------*/



var current_message_bchannel;
var current_channel;



function log_message_bchannel(m) {
  $('#message_bchannelMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_message_bchannel(m) {
  $('#message_bchannelMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}


function handle_message_bchannel_list_success(messages) {
    $("#message_bchanneltable").empty().append(update_message_bchannel_view(messages));
}

function on_error_message_bchannels(error) {
     error_message_bchannel("error request Messages -" + error.status);
}

function request_message_bchannels() {
  request_entities(messages_uri+"/channel/"+current_channel.id, handle_message_bchannel_list_success, on_error_message_bchannels);
}

function update_message_bchannel_view(messages) {
  if (current_message_bchannel == undefined)
    current_message_bchannel = messages[0]
  update_message_bchannel_form(current_message_bchannel);
  return _.template($("#message_bchannel-template").html(), {
    "messages": messages
  });
}


function update_message_bchannel_form(message) {
  try {
    //$('#name').val(message.name);
    //$('#email').val(message.email);
    current_message_bchannel = message;
    $('#message_bchannel_form').values(message);
    
    if (message.channel != undefined)
      $('#channel').val(message.channel.name);    //when not editable with a select box
    
    if (message.emitter != undefined)
      $('#emitter').val(message.emitter.name);  //when not editable with a select box
    
    if (message.dateString != undefined)
      $('#dateString').val(message.dateString);  //when not editable with a datepicker
    
    
  } catch (error) {
    error_message_bchannel("error update_message_bchannel_form");
  }
}



function message_bchannel_find(id) {
  request_entity(messages_uri + "/" + id,update_message_bchannel_form);
}


function init_message_bchannel_page() {
  current_channel =  JSON.parse(window.sessionStorage.getItem('current_channel'));
  //$('#from__id').val(current_channel.name); 
  document.getElementById("legend_message_bchannel").textContent = "Messages for channel "+current_channel.name;
  document.getElementById("message_bchannel_json_href").href = messages_uri+"/channel/"+current_channel.id; //see diff uri for rest && view
  document.getElementById("message_bchannel_json_label").textContent = "REST URL for message_bchannels by "+current_channel.name;
  document.getElementById("back_message_bchannel").href = window.sessionStorage.getItem('back_navigation')+".html";
  document.getElementById("back_message_bchannel").innerHTML = "back to "+window.sessionStorage.getItem('back_navigation');
  getNavbar("home", on_insert_navbar);
  request_message_bchannels();
}