/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for User  --------------*/

var user_example = {
  name: "Foo"
}

var current_user;

const users_uri = "rest/users";

function log_user(m) {
  $('#userMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_user(m) {
  $('#userMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}

function handle_user_error(error, mesg) {
  if ((error.status == 409) || (error.status == 400)) {
    error_user(mesg);
    var errorMsg = $.parseJSON(error.responseText);
    $.each(errorMsg, function(index, err) {
      document.getElementById("msg_" + index).innerHTML = err;
    });
  } else {
    error_user(error.status + ": " + error.statusText);
  }
}

function handle_user_success(user, mesg) {
  current_user = user;
  $('#user_form')[0].reset();
  log_user(mesg);
  request_users();
}

function handle_user_list_success(users) {
    $("#usertable").empty().append(update_user_view(users));
}

function on_success_user_update(user) {
  handle_user_success(user, "User Updated");
}

function on_success_user_add(user) {
  handle_user_success(user, "User Added");
}

function on_success_user_delete_byid(user) {
  handle_user_success(undefined, "User Deleted");
}

function on_error_user_delete_all(error) {
  handle_user_error(error,"error while deleting all Users!");
}

function on_error_user_delete_byid(error) {
 handle_user_error(error,"error while deleting User!");
}

function on_error_user_update(error) {
  handle_user_error(error,"error while updating User!");
}

function on_error_user_add(error) {
  handle_user_error(error,"error while adding User!");
}

function on_error_users(error) {
     error_user("error request Users -" + error.status);
}

function request_users() {
  request_entities(users_uri, handle_user_list_success, on_error_users);
}

function update_user_view(users) {
  if (current_user == undefined)
    current_user = users[0]
  on_success_user_form(current_user);
  return _.template($("#user-template").html(), {
    "users": users
  });
}

function on_success_user_delete_all(user) {
  current_user = undefined;
  $('#user_form')[0].reset();
  log_user("All Users Deleted");
  request_users();
}

function on_success_user_form(user) {
  try {
    //$('#name').val(user.name);
    //$('#email').val(user.email);
    current_user = user;
    $('#user_form').values(user);
  } catch (error) {
    error_user("error on_success_user_form");
  }
}

function on_error_user_form(err) {
  error_user(err.status + ": " + err.statusText);
}

function user_find(id) {
  request_entity(users_uri + "/" + id,on_success_user_form, on_error_user_form);
}

function user_delete(id) {
  delete_by_id(users_uri + "/" + id, on_success_user_delete_byid, on_error_user_delete_byid);
}

function clear_user_errors() {
  $('#userlayout').find("span.form_err").each(function(index) {
    $(this)[0].innerHTML = "";
  });
}

function init_user_page() {
  log_user("___init_user_page___");
  clear_user_errors();

  $('#user_form').submit(function(event) {
    var btn = document.activeElement.getAttribute('id');
    if (btn == "add")
      $('#id').val(-1);
    log_user("___user___" + btn);
    event.preventDefault();
    var userData = $(this).serializeUser();
    log_user("submit user : " + JSON.stringify(userData));
    clear_user_errors();
    if (btn == "add")
      update_entity(users_uri, "POST", userData, on_success_user_add, on_error_user_add);
    else
      update_entity(users_uri, "PUT", userData, on_success_user_update, on_error_user_update);
  });

  $("#user_refresh_button").click(function(event) {
    log_user("___refresh___");
    request_users();
  });

  $("#user_delete_button").click(function(event) {
    log_user("___delete___all");
    delete_all(users_uri + "/all", on_success_user_delete_all, on_error_user_delete_all);
  });

  $.fn.serializeUser = function() {
    var o = {};
    var form_data = $('#user_form').serializeArray();
    $.each(form_data, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }
  getNavbar("home", on_insert_navbar);
  try {
    request_users();
  } catch (error) {
    error_user("error request_users");
  }
}