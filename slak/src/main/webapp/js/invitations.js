/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for Invitation  --------------*/
var invitation_example = {
  id: 5,
  message: "Hello Jo Smith join Slak",
  invitationDate: 1608257208756,
  joinDate: null,
  emitter: {
    id: 1,
    name: "Martin",
    foreName: "Peter",
    email: "ji@jiji.com",
    nickName: "johnny",
    password: "qwerty",
    birthDate: null
  },
  receiver: {
    id: 2,
    name: "Smith",
    foreName: "Jo",
    email: "jo@jojo.com",
    nickName: "jamesbond",
    password: "azerty",
    birthDate: null
  },
  channel: {
    id: 2,
    name: "Dev",
    description: "development",
    privat: true,
    dateCreation: null
  }
 }


var invitation_channels_examples = [{
    id: 1,
    name: "Dev",
    description: "development",
    privat: true,
    dateCreation: null
  },
  {
    id: 1,
    name: "Devop",
    description: "development and operations",
    privat: true,
    dateCreation: null
  }
];


/*
    private String name;
    private String description;
    private boolean privat;
    private Date dateCreation;

*/

var invitation_users_examples = [{
    id: 1,
    name: "Martin",
    foreName: "Peter",
    email: "ji@jiji.com",
    nickName: "johnny",
    password: "qwerty",
    birthDat: null
  },
  {
    id: 2,
    name: "Smith",
    foreName: "Jo",
    email: "jo@jojo.com",
    nickName: "jamesbond",
    password: "azerty",
    birthDate: null
  },

  {
    id: 3,
    name: "Tom",
    foreName: "Tim",
    email: "ko@kojo.com",
    nickName: "ken",
    password: "foo",
    birthDate: null
  }
];


var current_invitation;
var current_user;



function log_invitation(m) {
  $('#invitationMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_invitation(m) {
  $('#invitationMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}

function handle_invitation_error(error, mesg) {
  if ((error.status == 409) || (error.status == 400)) {
    error_invitation(mesg);
    try {
      var errorMsg = $.parseJSON(error.responseText);
      $.each(errorMsg, function(index, err) {
        document.getElementById("msg_" + index).innerHTML = err;
      });
    } catch (cerror) {
      error_invitation(error.status + ": " + error.statusText);
    }
  } else {
    error_invitation(error.status + ": " + error.statusText);
  }
}

function handle_invitation_success(invitation, mesg) {
  current_invitation = invitation;
  $('#invitation_form')[0].reset();
  log_invitation(mesg);
  request_by_invitations();
}

function handle_invitation_list_success(invitations) {
  $("#invitationtable").empty().append(update_invitation_view(invitations));
}

function handle_for_invitation_list_success(invitations) {
	  $("#invitationfortable").empty().append(update_for_invitation_view(invitations));
}

function on_success_invitation_update(invitation) {
  handle_invitation_success(invitation, "Invitation Updated");
}

function on_success_invitation_add(invitation) {
  handle_invitation_success(invitation, "Invitation Added");
}

function on_success_invitation_delete_byid(invitation) {
  handle_invitation_success(undefined, "Invitation Deleted");
}

function on_error_invitation_delete_all(error) {
  handle_invitation_error(error, "error while deleting all Invitations!");
}

function on_error_invitation_delete_byid(error) {
  handle_invitation_error(error, "error while deleting Invitation!");
}

function on_error_invitation_update(error) {
  handle_invitation_error(error, "error while updating Invitation!");
}

function on_error_invitation_add(error) {
  handle_invitation_error(error, "error while adding Invitation!");
}

function on_error_invitations(error) {
  error_invitation("error request Invitations -" + error.status);
}

function request_by_invitations() {
  request_entities(invitations_uri + "/user/" + current_user.id, handle_invitation_list_success, on_error_invitations);
}


function request_for_invitations() {
	  request_entities(invitations_uri + "/foruser/" + current_user.id, handle_for_invitation_list_success, on_error_invitations);
}


function update_invitation_view(invitations) {
  if (current_invitation == undefined)
    current_invitation = invitations[0]
  update_invitation_form(current_invitation);
  return _.template($("#invitation-template").html(), {
    "invitations": invitations
  });
}

function update_for_invitation_view(for_invitations) {
	  return _.template($("#invitation-for-template").html(), {
	    "invitations_for": for_invitations
	  });
}

function on_success_invitation_delete_all(invitation) {
  current_invitation = undefined;
  $('#invitation_form')[0].reset();
  log_invitation("All Invitations Deleted");
  request_by_invitations();
}




function make_invitation_receiver_select() {
  var options = [];
  $.each(JSON.parse(window.sessionStorage.getItem("users")), function(id, user) {
    if (current_invitation.receiver != undefined)
      options.push('<option value="', user.id, user.id == current_invitation.receiver.id ? '" selected="">' : '">', user.name + " " + user.foreName, '</option>');
    else
      options.push('<option value="', user.id, '">', user.name + " " + user.foreName, '</option>');
  });
  $("#receiver").html(options.join(''));
}

function make_invitation_channel_select() {
  var options = [];
  $.each(JSON.parse(window.sessionStorage.getItem("channels")), function(id, channel) {
    if (current_invitation.channel != undefined)
      options.push('<option value="', channel.id, channel.id == current_invitation.channel.id ? '" selected="">' : '">', channel.name + '</option>');
    else
      options.push('<option value="', channel.id, '">', channel.name , '</option>');
  });
  $("#channel").html(options.join(''));
}


function update_invitation_form(invitation) {
  try {
    //$('#name').val(invitation.name);
    //$('#email').val(invitation.email);
    current_invitation = invitation;
    $('#invitation_form').values(invitation);
    if (invitation.emitter != undefined)
      $('#emitter').val(invitation.emitter.id);
    if (invitation.receiver != undefined)
      $('#receiver').val(invitation.receiver.id);
    if (invitation.channel != undefined)
      $('#channel').val(invitation.channel.id);     
    make_invitation_receiver_select();
    make_invitation_channel_select();
  } catch (error) {
    error_invitation("error update_invitation_form");
  }
}

function on_error_invitation_form(err) {
  error_invitation(err.status + ": " + err.statusText);
}

function invitation_find(id) {
  request_entity(invitations_uri + "/" + id, update_invitation_form, on_error_invitation_form);
}




function invitation_accept(id) {
	return $.ajax({
		url : invitations_uri+"/accept/"+id,
		dataType : 'json',
		type : "PUT",
		success : function(response) {
			log_invitation("invitation_accepted "+JSON.stringify(response));
		},
		error : function(err) {
			log_invitation("invitation_accept_failed "+JSON.stringify(err));
		}
	});
}

function invitation_delete(id) {
  delete_by_id(invitations_uri + "/" + id, on_success_invitation_delete_byid, on_error_invitation_delete_byid);
}

function clear_invitation_errors() {
  $('#invitationlayout').find("span.form_err").each(function(index) {
    $(this)[0].innerHTML = "";
  });
}

function send_invitation(entity) {
  $.ajax({
    url: invitations_uri,
    contentType: "application/json",
    dataType: "json",
    type: "PUT",
    data: JSON.stringify(entity),
    success: function(response) {
      log_invitation(response);
    },
    error: function(err) {
      error_invitation(err.responseText);
    }
  });
}


function invitation_test(uri) {
  $.ajax({
    url: uri,
    type: "POST",
    success: function(response) {
      log_invitation(response);
    },
    error: function(err) {
      error_invitation(err);
    }
  });
}



function init_invitation_page() {
  log_invitation("___init_invitation_page___");
  clear_invitation_errors();
  current_user = JSON.parse(window.sessionStorage.getItem('current_user'));
  $('#from_user_id').val(current_user.name);

  document.getElementById("invitation_json_href").href = invitations_uri + "/user/" + current_user.id;
  document.getElementById("invitation_json_label").textContent = "REST URL for invitations by " + current_user.name;
  document.getElementById("legend_invitation").textContent = "Invitations by user " + current_user.name;


  document.getElementById("back_invitation").href = window.sessionStorage.getItem('back_navigation')+".html";
  document.getElementById("back_invitation").innerHTML = "back to "+window.sessionStorage.getItem('back_navigation');



  $('#invitation_form').submit(function(event) {
    var btn = document.activeElement.getAttribute('id');
    if (btn == "add")
      $('#id').val(-1);

    log_invitation("___invitation___" + btn);
    event.preventDefault();
    var invitationData = $(this).serializeInvitation();

    make_invitation_receiver_select();
    make_invitation_channel_select();

    invitationData.emitter = {
      id: 0
    };

    invitationData.emitter.id = current_user.id;
    
    var receiver_id = invitationData.receiver;
    invitationData.receiver = {
      id: 0
    };
    invitationData.receiver.id = receiver_id;


    var channel_id = invitationData.channel;
    invitationData.channel = {
      id: 0
    };
    invitationData.channel.id = channel_id;


    log_invitation("submit invitation : " + JSON.stringify(invitationData));
    if (btn == "add")
      update_entity(invitations_uri, "POST", invitationData, on_success_invitation_add, on_error_invitation_add);
    else if (btn == "update")
      update_entity(invitations_uri, "PUT", invitationData, on_success_invitation_update, on_error_invitation_update);
    else if (btn == "send")
      send_invitation(invitationData);
    clear_invitation_errors();
  });

  $("#invitation_refresh_button").click(function(event) {
    log_invitation("___refresh___");
    request_by_invitations();
    request_for_invitations();
  });

  $("#invitation_delete_button").click(function(event) {
    log_invitation("___delete___all");
    delete_all(invitations_uri + "/all", on_success_invitation_delete_all, on_error_invitation_delete_all);
  });

  $("#invitation_test_button").click(function(event) {
    log_invitation("___test___");
    invitation_test("test7845");
  });

  $.fn.serializeInvitation = function() {
    var o = {};
    var form_data = $('#invitation_form').serializeArray();
    $.each(form_data, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }
  getNavbar("home", on_insert_navbar);
  try {
    request_by_invitations();
    request_for_invitations();
  } catch (error) {
    error_invitation("error request_by_invitations");
  }
}
//ok select receiver for invitations ok