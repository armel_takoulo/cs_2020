var TUTORIAL_SAVVY = {

	/* Makes Ajax calls to Servlet to download student Data */
	downloadStudentData : function() {
		var formattedstudentListArray = [];
		$.ajax({
			async : false,
			url : "StudentJsonDataServlet",
			dataType : "json",
			success : function(studentJsonData) {
				$.each(studentJsonData, function(index, aStudent) {
					formattedstudentListArray.push([ aStudent.mathematicsMark,
							aStudent.computerMark,
							(aStudent.mathematicsMark + aStudent.computerMark),
							aStudent.name ]);
				});
			}
		});
		return formattedstudentListArray;
	},

	/* Draws Bubble Chart For Student Data */
	drawStudentBubbleChart : function(formattedStudentJsonData) {
		$.jqplot.config.enablePlugins = true;
		$.jqplot('ts-student-chart', [ formattedStudentJsonData ], {
			title : 'Student Marks In Mathematic And Computer',
			seriesDefaults : {
				renderer : $.jqplot.BubbleRenderer,
				rendererOptions : {
					bubbleGradients : true
				},
				shadow : true
			}
		});
	}
};

$(document).ready(function() {
	var formatStudentData = TUTORIAL_SAVVY.downloadStudentData();
	TUTORIAL_SAVVY.drawStudentBubbleChart(formatStudentData);
});