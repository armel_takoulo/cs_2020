/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
Core JavaScript functionality for the application.  Performs the required
Restful calls, validates return values, and populates the member table.
 */

/* Builds the updated table for the member list */
function buildM_ember_Rows(members) {
    return _.template( $( "#member-tmpl" ).html(), {"members": members});
}

/* Uses JAX-RS GET to retrieve current member list */
function updateMemberTable() {
    // Display the loader widget
    $.mobile.loading("show");

    $.ajax({
        url: "rest/members",
        cache: false,
        success: function(data) {
            $( "#members" ).empty().append(buildMemberRows(data));
            $( "#member-table" ).table( "refresh" );
        },
        error: function(error) {
            //console.log("error updating table -" + error.status);
        },
        complete: function() {
            // Hide the loader widget
            $.mobile.loading("hide");
        }
    });
}




function updateChart() {
    var Points1 = [];
    for (var i=0; i<2*Math.PI; i+=1){ 
        Points1.push([i, Math.cos(i)]); 
    }
    var Points2 = []; 
    for (var i=0; i<2*Math.PI; i+=0.4){ 
        Points2.push([i, 2*Math.sin(i-.8)]); 
    }
    var Points3 = []; 
    for (var i=0; i<2*Math.PI; i+=1) { 
        Points3.push([i, 2.5 + Math.pow(i/4, 2)]); 
    }
    var Points4 = []; 
    for (var i=0; i<2*Math.PI; i+=1) { 
        Points4.push([i, -2.5 - Math.pow(i/4, 2)]); 
    } 
    alert("updateChart");
    /*
    $.jqplot ('chart_line', [Points1,Points2,Points3,Points4], {
        title: 'Line Chart With Options',
        seriesDefaults: {
            rendererOptions: {
                smooth: true
            }
        },
        series:[{
            lineWidth: 2,
            markerOptions:{ style:"diamond"}
        },{
            showLine: false,
            markerOptions:{style:"x", size:5}
        },{
            markerOptions:{style:"circle"}
        },{
            lineWidth: 5,
            markerOptions:{style:"filledSquare", size: 10}
        }],
        legend:{
            show: true,
            location:"e",
            placement:"outside"
        },
        axes: {
            xaxis: {
                label: "X-Axis",
                pad: 0 
            },
            yaxis: {
                label: "Y-Axis"
            }
        }
    });*/


}




/*
Attempts to register a new member using a JAX-RS POST.  The callbacks
the refresh the member table, or process JAX-RS response codes to update
the validation errors.
 */
function register___Member(memberData) {
    //clear existing  msgs
    $('span.invalid').remove();
    $('span.success').remove();

    // Display the loader widget
    $.mobile.loading("show");

    $.ajax({
        url: 'rest/members',
        contentType: "application/json",
        dataType: "json",
        type: "POST",
        data: JSON.stringify(memberData),
        success: function(data) {
            //console.log("Member registered");

            //clear input fields
            $('#reg')[0].reset();

            //mark success on the registration form
            $('#formMsgs').append($('<span class="success">Member Registered</span>'));

            updateMemberTable();
        },
        error: function(error) {
            if ((error.status == 409) || (error.status == 400)) {
                //console.log("Validation error registering user!");

                var errorMsg = $.parseJSON(error.responseText);

                $.each(errorMsg, function(index, val) {
                    $('<span class="invalid">' + val + '</span>').insertAfter($('#' + index));
                });
            } else {
                //console.log("error - unknown server issue");
                $('#formMsgs').append($('<span class="invalid">Unknown server error</span>'));
            }
        },
        complete: function() {
            // Hide the loader widget
            $.mobile.loading("hide");
        }
    });
}
