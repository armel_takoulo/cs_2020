
const GAIT_ADD_LABEL = "Add";
const GAIT_UPDATE_LABEL = "Update";

var gaitsCollection = [];

var gait = {
    id: 0,
    x_knee_o: 0,
    x_knee: 0,
    y_knee: 0,
    x_ankle_o: 0,
    x_ankle: 0,
    y_ankle: 0,
    x_hip: 0,
    y_hip: 0,
    dist_hip_base: 0,
    dist_knee_base: 0,
    name: "",
    introductionDate: "",
    url: ""
};


function CreateMockGaitData() {
    var gaitArray = [];
    var record0 = {
        id: 0,
        x_knee_o: 496.92,
        x_knee: 470.12,
        y_knee: 436.52,
        x_ankle_o: 248.82,
        x_ankle: 297.88,
        y_ankle: 90.75,
        x_hip: 508.69,
        y_hip: 943.04,
        dist_hip_base: 891.03,
        dist_knee_base: 425.57,
        name: "Foo",
        introductionDate: "06/11/2015",
        url: "http://bit.ly/1I8ZqZg"
    };

    gaitArray.push(record0);

    var record1 = {
        id: 1,
        x_knee_o: 49.92,
        x_knee: 670.12,
        y_knee: 236.52,
        x_ankle_o: 854.82,
        x_ankle: 52.241,
        y_ankle: 21.57,
        x_hip: 985.21,
        y_hip: 21.478,
        dist_hip_base: 321.25,
        dist_knee_base: 84.14,
        name: "Bar",
        introductionDate: "08/28/2015",
        url: "http://bit.ly/1j2dcrj"
    };

    gaitArray.push(record1);

    var record2 = {
        id: 2,
        x_knee_o: 85.214,
        x_knee: 36.21,
        y_knee: 54.21,
        x_ankle_o: 854.21,
        x_ankle: 4.21,
        y_ankle: 3.21,
        x_hip: 8.54,
        y_hip: 8.14,
        dist_hip_base: 52.14,
        dist_knee_base: 63.214,
        name: "Baz",
        introductionDate: "06/11/2015",
        url: "http://bit.ly/1I8ZqZg"
    };
    gaitArray.push(record2);
    return gaitArray;
}

function updateClick() {
    gait = (new Object());
    gait.id = $("#id").val();
    gait.x_knee_o = $("#x_knee_o").val();
    gait.x_knee = $("#x_knee").val();
    gait.y_knee = $("#y_knee").val();
    gait.x_ankle_o = $("#x_ankle_o").val();
    gait.x_ankle = $("#x_ankle").val();
    gait.x_hip = $("#x_hip").val();
    gait.dist_hip_base = $("#dist_hip_base").val();
    gait.dist_knee_base = $("#dist_knee_base").val();
    gait.name = $("#name").val();
    gait.introductionDate = $("#introdate").val();
    gait.url = $("#url").val();

    if ($("#updateButton").text().trim() == GAIT_ADD_LABEL) {
        var newId = Math.max.apply(Math, gaitsCollection.map(function (o) {
            return o.id;
        }));
        newId++;
        gait.id = newId;
        gaitAdd(gait);
    } else {
        gaitUpdate(gait);
    }
    $("#addButton").removeClass("hidden");
}

function formClear() {
    $("#x_knee_o").val(0.0);
    $("#x_knee").val(0.0);
    $("#y_knee").val(0.0);
    $("#x_ankle_o").val(0.0);
    $("#x_ankle").val(0.0);
    $("#x_hip").val(0.0);
    $("#y_hip").val(0.0);
    $("#y_ankle").val(0.0);
    $("#dist_hip_base").val(0.0);
    $("#dist_knee_base").val(0.0);
    $("#name").val("");
    $("#introdate").val("");
    $("#url").val("http://unknown");
    $("#name").val("noname");
}

function gaitUpdate(g) {
    $.each(gaitsCollection, function () {
        if (this.id == g.id) {
            this.x_knee_o = g.x_knee_o;
            this.x_knee = g.x_knee;
            this.y_knee = g.y_knee;
            this.y_ankle_o = g.y_ankle_o;
            this.x_ankle = g.x_ankle;
            this.x_hip = g.x_hip;
            this.dist_hip_base = g.dist_hip_base;
            this.dist_knee_base = g.dist_knee_base;
            this.name = g.name;
            this.introductionDate = g.introductionDate;
            this.url = g.url;
        }
    });
    gaitUpdateInTable(g);
}

function gaitGet(ctl) {
    var id = $(ctl).data("id");
    $("#id").val(id);
    var foundGait = $.grep(gaitsCollection, function (e) {
        return e.id == id;
    });
    gaitToFields(foundGait[0]);
    $("#editPanel").removeClass("hidden");
    $("#listPanel").addClass("hidden");
    $("#addButton").addClass("hidden");
    $("#updateButton").text(GAIT_UPDATE_LABEL);
}

function gaitToFields(g) {
    $("#x_knee_o").val(g.x_knee_o);
    $("#x_knee").val(g.x_knee);
    $("#y_knee").val(g.y_knee);
    $("#x_ankle_o").val(g.x_ankle_o);
    $("#x_ankle").val(g.x_ankle);
    $("#y_ankle").val(g.y_ankle);
    $("#x_hip").val(g.x_hip);
    $("#y_hip").val(g.y_hip);
    $("#dist_hip_base").val(g.dist_hip_base);
    $("#dist_knee_base").val(g.dist_knee_base);
    $("#name").val(g.name);
    $("#introdate").val(g.introductionDate);
    $("#url").val(g.url);
}

function gaitBuildTableRow(g) {
    var id = g.id;
    var x_knee_o = g.x_knee_o;
    var x_knee = g.x_knee;
    var y_knee = g.y_knee;
    var y_ankle = g.y_ankle;
    var x_ankle_o = g.x_ankle_o;
    var y_ankle = g.y_ankle;
    var x_ankle = g.x_ankle;
    var x_hip = g.x_hip;
    var y_hip = g.y_hip;
    var dist_hip_base = g.dist_hip_base;
    var dist_knee_base = g.dist_knee_base;
    var name = g.name;
    var introdate = g.introductionDate;
    var url = g.url;
    var htmlToReturn = "<tr>";


    htmlToReturn += "<td class='display-none'>" + id + "</td>";

    
    
    htmlToReturn += "<td>" + "<button type='button'  " +
        " onclick='gaitGet(this)' " + " class='btn btn-default kbutton-image kbutton-edit-24' " +
        " data-id='" + id + "'>";
    htmlToReturn += "</button>" + "</td>";

    htmlToReturn += "<td>" + "<button type='button'  " +
        " onclick='gaitDelete(this)' " + " class='btn btn-default kbutton-image kbutton-delete-16' " +
        " data-id='" + id + "'>";
    htmlToReturn += "</button>" + "</td>";



    htmlToReturn += "<td>" +
        name + "</td>" + "<td>" +
        x_knee_o + "</td>" + "<td>" +
        x_knee + "</td>" + "<td>" +
        y_knee + "</td>" + "<td>" +
        x_ankle_o + "</td>" + "<td>" +
        x_ankle + "</td>" + "<td>" +
        y_ankle + "</td>" + "<td>" +
        x_hip + "</td>" + "<td>" +
        y_hip + "</td>" + "<td>" +
        dist_hip_base + "</td>" + "<td>" +
        dist_knee_base + "</td>" + "<td>" +
        introdate + "</td>" + "<td>" +
        url + "</td>";

    htmlToReturn += + "</tr>";
    return htmlToReturn;
}

function gaitUpdateInTable(g) {
    // find gait , the associated button , keep the first praent 
    var row = $("#gaitTable button[data-id='" + g.id + "']").parents("tr")[0];
    $(row).after(gaitBuildTableRow(g));
    $(row).remove();
    formClear();
    $("#updateButton").text(GAIT_ADD_LABEL);
    $("#editPanel").addClass("hidden");
    $("#listPanel").removeClass("hidden");
    selectCurrent();
}

function gaitAdd(g) {
    gaitsCollection.push(g);
    gaitAddDone(g);
}

function gaitAddDone(g) {
    gaitAddRow(g);
    $("#editPanel").addClass("hidden");
    $("#listPanel").removeClass("hidden");
    formClear();
    selectCurrent();
}

function gaitAddRow(g) {
    if ($("#gaitTable tbody").length == 0) {
        $("#gaitTable").append("<tbody></tbody>");
    }
    var rowhtml = gaitBuildTableRow(g);
    $("#gaitTable tbody").append(rowhtml);
}

function cancelClick() {
    formClear();
    $("#editPanel").addClass("hidden");
    $("#listPanel").removeClass("hidden");
    $("#addButton").removeClass("hidden");
}



function gaitDelete(ctl) {
    var id = $(ctl).data("id");
    var data = $.grep(gaitsCollection, function (e) {
        return e.id !== id;
    });
    gaitsCollection = data;
    $(ctl).parents("tr").remove();
}

function gaitListDone(gaits) {
    $.each(gaits, function (index, g) {
        // add a row to the gait table
        gaitAddRow(g);
    });
    gait = gaitsCollection[0];
    selectCurrent();
};

function gaitList() {
    if (gaitsCollection.length == 0)
        gaitsCollection = CreateMockGaitData();
    gaitListDone(gaitsCollection);
}

function addClick() {
    $("#editPanel").removeClass("hidden");
    $("#listPanel").addClass("hidden");
    $("#addButton").addClass("hidden");
    $("#updateButton").text(GAIT_ADD_LABEL);
    formClear();
}

function selectGaitRow(row) {
    row.addClass("display-select").siblings().removeClass("display-select");
}

function setGaitAllRowsClickable() {
    $('#gaitTable tbody tr').click(function () {
        selectGaitRow($(this));
    });
}

function setGaitRowClickable(row) {
    row.click(function () {
        selectGaitRow($(this));
    });
}

function selectCurrent() {
    $('#gaitTable tbody tr').each(function () {
        var row = $(this);
        var cel1 = row.find("td:eq(0)");
        var gid = cel1.text();
        if (gid == gait.id) {
            selectGaitRow(row);
            setGaitRowClickable(row);
        }
    });
}


// see https://www.codemag.com/Article/1601031/CRUD-in-HTML-JavaScript-and-jQuery-Using-the-Web-API

function productList() {
    // Call Web API to get a list of Product
    $.ajax({
        url: '/api/Product/',
        type: 'GET',
        dataType: 'json',
        success: function (products) {
            productListSuccess(products);
        },
        error: function (request, message, error) {
            handleException(request, message, error);
        }
    });
}

function productListSuccess(products) {
    // Iterate over the collection of data
    $.each(products, function (index, product) {
        // Add a row to the Product table
        productAddRow(product);
    });
}


function productAdd(product) {
    $.ajax({
        url: "/api/Product",
        type: 'POST',
        contentType:
            "application/json;charset=utf-8",
        data: JSON.stringify(product),
        success: function (product) {
            productAddSuccess(product);
        },
        error: function (request, message, error) {
            handleException(request, message, error);
        }
    });
}

function productAddRow(product) {
    // Check if <tbody> tag exists, add one if not
    if ($("#productTable tbody").length == 0) {
        $("#productTable").append("<tbody></tbody>");
    }
    // Append row to <table>
    $("#productTable tbody").append(
        productBuildTableRow(product));
}

function handleException(request, message,
    error) {
    var msg = "";
    msg += "Code: " + request.status + "\n";
    msg += "Text: " + request.statusText + "\n";
    if (request.responseJSON != null) {
        msg += "Message" +
            request.responseJSON.Message + "\n";
    }
    alert(msg);
}



